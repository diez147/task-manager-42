package ru.tsc.babeshko.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.babeshko.tm.api.endpoint.*;
import ru.tsc.babeshko.tm.api.repository.ICommandRepository;
import ru.tsc.babeshko.tm.api.service.*;
import ru.tsc.babeshko.tm.command.AbstractCommand;
import ru.tsc.babeshko.tm.command.system.ExitCommand;
import ru.tsc.babeshko.tm.exception.system.UnknownArgumentException;
import ru.tsc.babeshko.tm.exception.system.UnknownCommandException;
import ru.tsc.babeshko.tm.repository.CommandRepository;
import ru.tsc.babeshko.tm.service.CommandService;
import ru.tsc.babeshko.tm.service.LoggerService;
import ru.tsc.babeshko.tm.service.PropertyService;
import ru.tsc.babeshko.tm.service.TokenService;
import ru.tsc.babeshko.tm.util.SystemUtil;
import ru.tsc.babeshko.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.tsc.babeshko.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final ITokenService tokenService = new TokenService();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @NotNull
    private final String host = propertyService.getServerHost();

    @NotNull
    private final String port = Integer.toString(propertyService.getServerPort());

    @Getter
    @NotNull
    private final ISystemEndpoint systemEndpoint = ISystemEndpoint.newInstance(host, port);

    @Getter
    @NotNull
    private final IDomainEndpoint domainEndpoint = IDomainEndpoint.newInstance(host, port);

    @Getter
    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(host, port);

    @Getter
    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = IProjectTaskEndpoint.newInstance(host, port);

    @Getter
    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(host, port);

    @Getter
    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(host, port);

    @Getter
    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(host, port);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = clazz.newInstance();
        registry(command);
    }


    private void prepareStartup() {
        initPID();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void run(@Nullable final String[] args) {
        if (processArgument(args)) new ExitCommand().execute();
        prepareStartup();
        //runTestRequests();
        while (true) processCommand();
    }

    private void processCommand() {
        try {
            System.out.println("ENTER COMMAND:");
            @NotNull final String command = TerminalUtil.nextLine();
            processCommand(command);
            loggerService.command(command);
            System.out.println("[OK]");
        } catch (final Exception e) {
            loggerService.error(e);
        }
    }

    public void processCommand(@NotNull final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException(command);
        abstractCommand.execute();
    }

    private void processArgument(@Nullable final String arg) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new UnknownArgumentException(arg);
        abstractCommand.execute();
    }

    private boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    /*private void runTestRequests() {
        @NotNull final String adminId = "88de1940-54ad-42e7-b272-c2094fdd949b";
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = Integer.toString(propertyService.getServerPort());

        @NotNull final ISystemEndpoint systemEndpoint = ISystemEndpoint.newInstance(host, port);
        @NotNull final ServerVersionRequest versionRequest = new ServerVersionRequest();
        System.out.println(systemEndpoint.getVersion(versionRequest).getVersion());

        @NotNull final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(host, port);
        @NotNull final UserProfileRequest userProfileRequest = new UserProfileRequest();
        userProfileRequest.setUserId(adminId);
        @Nullable final User user = userEndpoint.showProfileUser(userProfileRequest).getUser();
        System.out.println(user.getLogin());

        @NotNull final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(host, port);
        @NotNull final ProjectShowByIndexRequest projectShowByIndexRequest = new ProjectShowByIndexRequest(1);
        projectShowByIndexRequest.setUserId(adminId);
        System.out.println(projectEndpoint.showProjectByIndex(projectShowByIndexRequest).getProject());

        @NotNull final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(host, port);
        @NotNull final TaskShowByIndexRequest taskShowByIndexRequest = new TaskShowByIndexRequest(1);
        taskShowByIndexRequest.setUserId(adminId);
        System.out.println(taskEndpoint.showTaskByIndex(taskShowByIndexRequest).getTask());

        @NotNull final IDomainEndpoint domainEndpoint = IDomainEndpoint.newInstance(host, port);
        @NotNull final DataJsonSaveFasterXmlRequest dataJsonSaveFasterXmlRequest = new DataJsonSaveFasterXmlRequest();
        dataJsonSaveFasterXmlRequest.setUserId(adminId);
        domainEndpoint.saveDataJsonFasterXml(dataJsonSaveFasterXmlRequest);
    }*/

}