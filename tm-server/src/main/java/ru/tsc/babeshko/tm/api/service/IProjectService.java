package ru.tsc.babeshko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.enumerated.Status;
import ru.tsc.babeshko.tm.dto.model.ProjectDTO;

import java.util.Date;

public interface IProjectService extends IUserOwnedService<ProjectDTO> {

    void create(@NotNull String userId, @NotNull String name);

    void create(@NotNull String userId, @NotNull String name, @NotNull String description);

    void create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    );

    @NotNull
    ProjectDTO updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description);

    @NotNull
    ProjectDTO changeProjectStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status);

}