package ru.tsc.babeshko.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.api.repository.IProjectRepository;
import ru.tsc.babeshko.tm.api.repository.ITaskRepository;
import ru.tsc.babeshko.tm.api.service.IConnectionService;
import ru.tsc.babeshko.tm.api.service.IProjectService;
import ru.tsc.babeshko.tm.api.service.IProjectTaskService;
import ru.tsc.babeshko.tm.api.service.ITaskService;
import ru.tsc.babeshko.tm.dto.model.ProjectDTO;
import ru.tsc.babeshko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.babeshko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.babeshko.tm.exception.field.EmptyIdException;
import ru.tsc.babeshko.tm.exception.field.EmptyUserIdException;
import ru.tsc.babeshko.tm.dto.model.TaskDTO;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;


    public ProjectTaskService(
            @NotNull IConnectionService connectionService,
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService

    ) {
        this.connectionService = connectionService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @NotNull
    private SqlSession getSession() {
        return connectionService.getSqlSession();
    }

    @Override
    public void bindTaskToProject(
            @NotNull String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        if (taskId.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            @Nullable final TaskDTO task = taskService.findOneById(userId, taskId);
            @Nullable final ProjectDTO project = projectService.findOneById(userId, projectId);
            if (project == null) throw new ProjectNotFoundException();
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(projectId);
            taskRepository.update(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void unbindTaskFromProject(
            @NotNull String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        if (taskId.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            @Nullable final TaskDTO task = taskService.findOneById(userId, taskId);
            @Nullable final ProjectDTO project = projectService.findOneById(userId, projectId);
            if (project == null) throw new ProjectNotFoundException();
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(null);
            taskRepository.update(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeProjectById(@NotNull String userId, @NotNull final String projectId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = getSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            @Nullable final ProjectDTO project = projectService.findOneById(userId, projectId);
            if (project == null) throw new ProjectNotFoundException();
            taskRepository.removeAllByProjectId(userId, projectId);
            projectRepository.remove(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}